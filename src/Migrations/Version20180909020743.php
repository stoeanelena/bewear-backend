<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180909020743 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_users DROP FOREIGN KEY FK_C2502824727ACA70');
        $this->addSql('DROP INDEX IDX_C2502824727ACA70 ON app_users');
        $this->addSql('ALTER TABLE app_users CHANGE parent_id protector_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE app_users ADD CONSTRAINT FK_C250282485C04254 FOREIGN KEY (protector_id) REFERENCES app_users (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_C250282485C04254 ON app_users (protector_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_users DROP FOREIGN KEY FK_C250282485C04254');
        $this->addSql('DROP INDEX IDX_C250282485C04254 ON app_users');
        $this->addSql('ALTER TABLE app_users CHANGE protector_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE app_users ADD CONSTRAINT FK_C2502824727ACA70 FOREIGN KEY (parent_id) REFERENCES app_users (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_C2502824727ACA70 ON app_users (parent_id)');
    }
}
