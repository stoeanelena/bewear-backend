<?php
// src/Services/UserStatusService.php
namespace App\Service;

use App\Entity\User;
use App\Entity\UserStatus;
use App\Kernel;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Dotenv\Dotenv;

class UserStatusService
{
    /** @var ManagerRegistry */
    private $doctrine;

    const API_URL = 'https://fcm.googleapis.com/fcm/send';
    const API_KEY = 'key=AAAAYuJwDw4:APA91bEvzazNNVSflRYz5JJRxIPjpWJRUvls_JXLc9AITzD0EIpGi5a6QEukafFtz24i-0V7ddIgZDFnjC0p2hf8abArPgsaonOs3MC5GEqxZ7m7N9-BTNYdki_kfng1QhS6eqWOPgHY';

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getDoctrine(): ManagerRegistry
    {
        return $this->doctrine;
    }

    public function saveUserStatus(
        User $user,
        string $status,
        float $latitude,
        float $longitude
    ) {

        /** @var EntityManager $emInstance */
        $entityManager = $this->getDoctrine()->getManager();

        if (!in_array($status, UserStatus::STATUSES)) {
            throw new \Exception('Invalid status!');
        }

        $userStatus = (new UserStatus())->setUser($user)
            ->setStatus($status)
            ->setLatitude($latitude)
            ->setLongitude($longitude)
            ->setTimestamp(new \DateTime());

        $entityManager->persist($userStatus);

        $entityManager->flush();

        return $userStatus;
    }

    public function alertProtector(UserStatus $userStatus): bool
    {
        if (empty($userStatus->getUser()->getParentId())) {
            throw new \Exception('Invalid protector');
        }

        /** @var EntityManager $emInstance */
        $entityManager = $this->getDoctrine()->getManager();

        $userRepository = $entityManager->getRepository(User::class);

        /** @var User $protectorUser */
        $protectorUser = $userRepository->find($userStatus->getUser()->getParentId());

        if (empty($protectorUser) || empty($protectorUser->getDeviceToken())) {
            return false;
        }

        $data = [
            'to' => $protectorUser->getDeviceToken(),
            'notification' => [
                'title' => 'Notification title',
                'body' => 'Notification body',
            ],
            'data' => [
                'userId' => $userStatus->getUser()->getId(),
                'latitude' => $userStatus->getLatitude(),
                'longitude' => $userStatus->getLongitude(),
            ],
        ];

        $dataString = json_encode($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => self::API_URL,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $dataString
        ));

        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            [
                'Authorization:' . self::API_KEY,
                'Content-type:application/json',
                'Content-Length: ' . strlen($dataString),
            ]
        );

        $response = curl_exec($curl);

        if (empty($response)) {
            return false;
        }

        $resultData = json_decode($response, true);

        if (empty($resultData)) {
            return false;
        }

        $result = reset($resultData['results']);

        if (empty($result['error'])) {
            return false;
        }

        return true;
    }
}