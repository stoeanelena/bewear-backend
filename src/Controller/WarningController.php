<?php
// src/Controller/WarningController.php
namespace App\Controller;

use App\Entity\User;
use App\Entity\UserStatus;
use App\Service\UserStatusService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class WarningController extends AbstractController
{
    public function receive(Request $request, UserStatusService $userStatusService)
    {
        $requestData = $request->getContent();

        $responseData = [
            'isError' => false,
            'errorMessage' => null,
            'data' => [],
        ];

        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');

        try {
            $data = json_decode($requestData, true);

            if (empty($data)) {
                throw new \Exception('Invalid request format!');
            }

            $userId = intval($data['userId']) ?? null;
            $latitude = $data['latitude'] ?? null;
            $longitude = $data['longitude'] ?? null;

            if (empty($userId) || empty($latitude) || empty($longitude)) {
                throw new \Exception('Missing required data!');
            }

            $status = $data['status'] ?? UserStatus::USER_STATUS_DEFAULT;

            $entityManager = $this->getDoctrine()->getManager();

            $userRepository = $entityManager->getRepository(User::class);

            /** @var User $user */
            $user = $userRepository->find($userId);

            if (empty($user)) {
                throw new \Exception('User/device not found!');
            }

            if ($user->getRoles() != User::ROLE_BEWEAR) {
                throw new \Exception('Invalid role!');
            }

            /** @var UserStatus $userStatus */
            $userStatus = $userStatusService->saveUserStatus($user, $status, $latitude, $longitude);

            if (!$userStatusService->alertProtector($userStatus)) {
                throw new \Exception('Notification data was saved but the protector was not alerted');
            }

            $responseData['data']['message'] = 'Protector succesfully alerted!';
        } catch (\Exception $e) {
            $responseData['isError'] = true;
            $responseData['errorMessage'] = $e->getMessage();
        }

        $response->setContent(json_encode($responseData));

        return $response;
    }
}